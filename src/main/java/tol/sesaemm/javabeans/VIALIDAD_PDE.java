/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class VIALIDAD_PDE {

    private String tipo_vial;
    private String nom_vial;

    public String getTipo_vial()
      {
        return tipo_vial;
      }

    public void setTipo_vial(String tipo_vial)
      {
        this.tipo_vial = tipo_vial;
      }

    public String getNom_vial()
      {
        return nom_vial;
      }

    public void setNom_vial(String nom_vial)
      {
        this.nom_vial = nom_vial;
      }

}
