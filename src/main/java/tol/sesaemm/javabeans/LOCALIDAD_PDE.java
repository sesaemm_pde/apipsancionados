/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class LOCALIDAD_PDE {
    
    private String nom_loc;
    private String cve_loc;

    public String getNom_loc()
      {
        return nom_loc;
      }

    public void setNom_loc(String nom_loc)
      {
        this.nom_loc = nom_loc;
      }

    public String getCve_loc()
      {
        return cve_loc;
      }

    public void setCve_loc(String cve_loc)
      {
        this.cve_loc = cve_loc;
      }

}
