/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class RESPONSABLE_SANCION
  {
    private String nombres;
    private String primerApellido;
    private String segundoApellido;

    public RESPONSABLE_SANCION()
      {
      }

    public String getNombres()
      {
        return nombres;
      }

    public void setNombres(String nombres)
      {
        this.nombres = nombres;
      }

    public String getPrimerApellido()
      {
        return primerApellido;
      }

    public void setPrimerApellido(String primerApellido)
      {
        this.primerApellido = primerApellido;
      }

    public String getSegundoApellido()
      {
        return segundoApellido;
      }

    public void setSegundoApellido(String segundoApellido)
      {
        this.segundoApellido = segundoApellido;
      }
    
  }
