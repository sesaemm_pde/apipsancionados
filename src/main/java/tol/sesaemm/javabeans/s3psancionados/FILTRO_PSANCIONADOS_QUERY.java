/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class FILTRO_PSANCIONADOS_QUERY implements Serializable {
    
    private String id;
    private String nombreRazonSocial;
    private String rfc;
    private String institucionDependencia;
    private String expediente;
    private ArrayList<String> tipoSancion;
    private String tipoPersona;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        this.nombreRazonSocial = nombreRazonSocial;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getInstitucionDependencia() {
        return institucionDependencia;
    }

    public void setInstitucionDependencia(String institucionDependencia) {
        this.institucionDependencia = institucionDependencia;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public ArrayList<String> getTipoSancion() {
        return tipoSancion;
    }

    public void setTipoSancion(ArrayList<String> tipoSancion) {
        this.tipoSancion = tipoSancion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
}
