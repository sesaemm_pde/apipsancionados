/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class FILTRO_PSANCIONADOS_WS implements Serializable {
    
    private FILTRO_PSANCIONADOS_SORT sort;  
    private int page;
    private int pageSize;
    private FILTRO_PSANCIONADOS_QUERY query;
    private String rfcSolicitante;

    public FILTRO_PSANCIONADOS_SORT getSort() {
        return sort;
    }

    public void setSort(FILTRO_PSANCIONADOS_SORT sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public FILTRO_PSANCIONADOS_QUERY getQuery() {
        return query;
    }

    public void setQuery(FILTRO_PSANCIONADOS_QUERY query) {
        this.query = query;
    }

    public String getRfcSolicitante() {
        return rfcSolicitante;
    }

    public void setRfcSolicitante(String rfcSolicitante) {
        this.rfcSolicitante = rfcSolicitante;
    }
}
