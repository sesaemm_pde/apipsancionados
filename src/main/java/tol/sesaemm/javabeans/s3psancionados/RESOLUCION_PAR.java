/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class RESOLUCION_PAR
  {
    private String sentido;
    private String url;
    private String fechaNotificacion;

    public String getSentido()
      {
        return sentido;
      }

    public void setSentido(String sentido)
      {
        this.sentido = sentido;
      }

    public String getUrl()
      {
        return url;
      }

    public void setUrl(String url)
      {
        this.url = url;
      }

    public String getFechaNotificacion()
      {
        return fechaNotificacion;
      }

    public void setFechaNotificacion(String fechaNotificacion)
      {
        this.fechaNotificacion = fechaNotificacion;
      }
    
  }
