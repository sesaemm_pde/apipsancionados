/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

import java.util.ArrayList;
import tol.sesaemm.javabeans.PAGINATION;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class PSANCIONADOSROOT
  {

    private PAGINATION pagination;
    private ArrayList<PSANCIONADOS> results;

    public PAGINATION getPagination()
      {
        return pagination;
      }

    public void setPagination(PAGINATION pagination)
      {
        this.pagination = pagination;
      }

    public ArrayList<PSANCIONADOS> getResults()
      {
        return results;
      }

    public void setResults(ArrayList<PSANCIONADOS> results)
      {
        this.results = results;
      }
    
  }
