package tol.sesaemm.javabeans.s3psancionados;

import tol.sesaemm.javabeans.PAIS;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DOMICILIO_MEXICO
  {

    private PAIS pais;
    private ENTIDAD_FEDERATIVA entidadFederativa;
    private MUNICIPIO municipio;
    private String codigoPostal;
    private LOCALIDAD localidad;
    private VIALIDAD vialidad;
    private String numeroExterior;
    private String numeroInterior;

    public PAIS getPais()
      {
        return pais;
      }

    public void setPais(PAIS pais)
      {
        this.pais = pais;
      }

    public ENTIDAD_FEDERATIVA getEntidadFederativa()
      {
        return entidadFederativa;
      }

    public void setEntidadFederativa(ENTIDAD_FEDERATIVA entidadFederativa)
      {
        this.entidadFederativa = entidadFederativa;
      }

    public MUNICIPIO getMunicipio()
      {
        return municipio;
      }

    public void setMunicipio(MUNICIPIO municipio)
      {
        this.municipio = municipio;
      }

    public String getCodigoPostal()
      {
        return codigoPostal;
      }

    public void setCodigoPostal(String codigoPostal)
      {
        this.codigoPostal = codigoPostal;
      }

    public LOCALIDAD getLocalidad()
      {
        return localidad;
      }

    public void setLocalidad(LOCALIDAD localidad)
      {
        this.localidad = localidad;
      }

    public VIALIDAD getVialidad()
      {
        return vialidad;
      }

    public void setVialidad(VIALIDAD vialidad)
      {
        this.vialidad = vialidad;
      }

    public String getNumeroExterior()
      {
        return numeroExterior;
      }

    public void setNumeroExterior(String numeroExterior)
      {
        this.numeroExterior = numeroExterior;
      }

    public String getNumeroInterior()
      {
        return numeroInterior;
      }

    public void setNumeroInterior(String numeroInterior)
      {
        this.numeroInterior = numeroInterior;
      }

  }
