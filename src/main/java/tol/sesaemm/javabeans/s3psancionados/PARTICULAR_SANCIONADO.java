/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class PARTICULAR_SANCIONADO
  {
    private String nombreRazonSocial;
    private String objetoSocial;
    private String rfc;
    private String tipoPersona;
    private String telefono;
    private DOMICILIO_MEXICO domicilioMexico;
    private DOMICILIO_EXTRANJERO domicilioExtranjero;
    private PERSONA directorGeneral;
    private PERSONA apoderadoLegal;

    public String getNombreRazonSocial()
      {
        return nombreRazonSocial;
      }

    public void setNombreRazonSocial(String nombreRazonSocial)
      {
        this.nombreRazonSocial = nombreRazonSocial;
      }

    public String getObjetoSocial()
      {
        return objetoSocial;
      }

    public void setObjetoSocial(String objetoSocial)
      {
        this.objetoSocial = objetoSocial;
      }

    public String getRfc()
      {
        return rfc;
      }

    public void setRfc(String rfc)
      {
        this.rfc = rfc;
      }

    public String getTipoPersona()
      {
        return tipoPersona;
      }

    public void setTipoPersona(String tipoPersona)
      {
        this.tipoPersona = tipoPersona;
      }

    public String getTelefono()
      {
        return telefono;
      }

    public void setTelefono(String telefono)
      {
        this.telefono = telefono;
      }

    public DOMICILIO_MEXICO getDomicilioMexico()
      {
        return domicilioMexico;
      }

    public void setDomicilioMexico(DOMICILIO_MEXICO domicilioMexico)
      {
        this.domicilioMexico = domicilioMexico;
      }

    public DOMICILIO_EXTRANJERO getDomicilioExtranjero()
      {
        return domicilioExtranjero;
      }

    public void setDomicilioExtranjero(DOMICILIO_EXTRANJERO domicilioExtranjero)
      {
        this.domicilioExtranjero = domicilioExtranjero;
      }

    public PERSONA getDirectorGeneral()
      {
        return directorGeneral;
      }

    public void setDirectorGeneral(PERSONA directorGeneral)
      {
        this.directorGeneral = directorGeneral;
      }

    public PERSONA getApoderadoLegal()
      {
        return apoderadoLegal;
      }

    public void setApoderadoLegal(PERSONA apoderadoLegal)
      {
        this.apoderadoLegal = apoderadoLegal;
      }

  }
