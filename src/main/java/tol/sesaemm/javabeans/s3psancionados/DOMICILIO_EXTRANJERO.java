/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3psancionados;

import tol.sesaemm.javabeans.PAIS;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DOMICILIO_EXTRANJERO
  {
    private String calle;
    private String numeroExterior;
    private String numeroInterior;
    private String ciudadLocalidad;
    private String estadoProvincia;
    private PAIS pais;
    private String codigoPostal;

    public String getCalle()
      {
        return calle;
      }

    public void setCalle(String calle)
      {
        this.calle = calle;
      }

    public String getNumeroExterior()
      {
        return numeroExterior;
      }

    public void setNumeroExterior(String numeroExterior)
      {
        this.numeroExterior = numeroExterior;
      }

    public String getNumeroInterior()
      {
        return numeroInterior;
      }

    public void setNumeroInterior(String numeroInterior)
      {
        this.numeroInterior = numeroInterior;
      }

    public String getCiudadLocalidad()
      {
        return ciudadLocalidad;
      }

    public void setCiudadLocalidad(String ciudadLocalidad)
      {
        this.ciudadLocalidad = ciudadLocalidad;
      }

    public String getEstadoProvincia()
      {
        return estadoProvincia;
      }

    public void setEstadoProvincia(String estadoProvincia)
      {
        this.estadoProvincia = estadoProvincia;
      }

    public PAIS getPais()
      {
        return pais;
      }

    public void setPais(PAIS pais)
      {
        this.pais = pais;
      }

    public String getCodigoPostal()
      {
        return codigoPostal;
      }

    public void setCodigoPostal(String codigoPostal)
      {
        this.codigoPostal = codigoPostal;
      }
    
  }
