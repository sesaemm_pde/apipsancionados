/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class INSTITUCION_DEPENDENCIA implements Serializable
  {
    private String nombre;
    private String siglas;
    private String clave;

    public String getNombre()
      {
        return nombre;
      }

    public void setNombre(String nombre)
      {
        this.nombre = nombre;
      }

    public String getSiglas()
      {
        return siglas;
      }

    public void setSiglas(String siglas)
      {
        this.siglas = siglas;
      }

    public String getClave()
      {
        return clave;
      }

    public void setClave(String clave)
      {
        this.clave = clave;
      }
    
    
  }
