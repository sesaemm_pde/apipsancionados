/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SISTEMA implements Serializable
  {
    @BsonId
    @BsonProperty("_id")  
    private ObjectId idSpdn;
    private Integer id;
    private String descripcion;
    private String contexto;
    private String siglas;
    private Integer activo;
    private Integer plataforma;

    public SISTEMA()
      {
      }
    
    public ObjectId getIdSpdn()
      {
        return idSpdn;
      }

    public void setIdSpdn(ObjectId idSpdn)
      {
        this.idSpdn = idSpdn;
      }

    public Integer getId()
      {
        return id;
      }

    public void setId(Integer id)
      {
        this.id = id;
      }

    public String getDescripcion()
      {
        return descripcion;
      }

    public void setDescripcion(String descripcion)
      {
        this.descripcion = descripcion;
      }

    public String getContexto()
      {
        return contexto;
      }

    public void setContexto(String contexto)
      {
        this.contexto = contexto;
      }

    public String getSiglas()
      {
        return siglas;
      }

    public void setSiglas(String siglas)
      {
        this.siglas = siglas;
      }

    public Integer getActivo()
      {
        return activo;
      }

    public void setActivo(Integer activo)
      {
        this.activo = activo;
      }

    public Integer getPlataforma()
      {
        return plataforma;
      }

    public void setPlataforma(Integer plataforma)
      {
        this.plataforma = plataforma;
      }
    
  }
