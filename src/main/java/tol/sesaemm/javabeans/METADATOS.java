/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class METADATOS
  {
    private String usuario_registro;
    private String fecha_registro;
    private String usuario_modificacion;
    private String fecha_modificacion;
    private String usuario_publicacion;
    private String fecha_publicacion;
    private String usuario_despublicacion;
    private String fecha_despublicacion;

    public METADATOS()
      {
        this.usuario_registro = "";
        this.fecha_registro = "";
        this.usuario_modificacion = "";
        this.fecha_modificacion = "";
        this.usuario_publicacion = "";
        this.fecha_publicacion = "";
        this.usuario_despublicacion = "";
        this.fecha_despublicacion = "";
      }

    public METADATOS(
            @BsonProperty("usuario_registro") 
            String usuario_registro, 
            @BsonProperty("fecha_registro") 
            String fecha_registro, 
            @BsonProperty("usuario_modificacion") 
            String usuario_modificacion, 
            @BsonProperty("fecha_modificacion") 
            String fecha_modificacion, 
            @BsonProperty("usuario_publicacion") 
            String usuario_publicacion,
            @BsonProperty("fecha_publicacion")  
            String fecha_publicacion, 
            @BsonProperty("usuario_despublicacion") 
            String usuario_despublicacion, 
            @BsonProperty("fecha_despublicacion") 
            String fecha_despublicacion
    )
      {
        this.usuario_registro = usuario_registro;
        this.fecha_registro = fecha_registro;
        this.usuario_modificacion = usuario_modificacion;
        this.fecha_modificacion = fecha_modificacion;
        this.usuario_publicacion = usuario_publicacion;
        this.fecha_publicacion = fecha_publicacion;
        this.usuario_despublicacion = usuario_despublicacion;
        this.fecha_despublicacion = fecha_despublicacion;
      }
    
    public String getUsuario_registro()
      {
        return usuario_registro;
      }

    public void setUsuario_registro(String usuario_registro)
      {
        this.usuario_registro = usuario_registro;
      }

    public String getFecha_registro()
      {
        return fecha_registro;
      }

    public void setFecha_registro(String fecha_registro)
      {
        this.fecha_registro = fecha_registro;
      }

    public String getUsuario_modificacion()
      {
        return usuario_modificacion;
      }

    public void setUsuario_modificacion(String usuario_modificacion)
      {
        this.usuario_modificacion = usuario_modificacion;
      }

    public String getFecha_modificacion()
      {
        return fecha_modificacion;
      }

    public void setFecha_modificacion(String fecha_modificacion)
      {
        this.fecha_modificacion = fecha_modificacion;
      }

    public String getUsuario_publicacion()
      {
        return usuario_publicacion;
      }

    public void setUsuario_publicacion(String usuario_publicacion)
      {
        this.usuario_publicacion = usuario_publicacion;
      }

    public String getFecha_publicacion()
      {
        return fecha_publicacion;
      }

    public void setFecha_publicacion(String fecha_publicacion)
      {
        this.fecha_publicacion = fecha_publicacion;
      }

    public String getUsuario_despublicacion()
      {
        return usuario_despublicacion;
      }

    public void setUsuario_despublicacion(String usuario_despublicacion)
      {
        this.usuario_despublicacion = usuario_despublicacion;
      }

    public String getFecha_despublicacion()
      {
        return fecha_despublicacion;
      }

    public void setFecha_despublicacion(String fecha_despublicacion)
      {
        this.fecha_despublicacion = fecha_despublicacion;
      }
    
  }
