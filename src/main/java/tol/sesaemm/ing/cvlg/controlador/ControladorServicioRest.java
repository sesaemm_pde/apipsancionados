package tol.sesaemm.ing.cvlg.controlador;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@RestController
public class ControladorServicioRest
{

    /**
     * Mensaje principale del web service
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String webapp()
    {
        return "Servicio Web de conexión SESAEMM - SESNA";
    }

    /**
     * Obtiene en formato Json estructura peticion Post (atributos de paginacion
     * y resultados de consulta) de particulares sancionados
     *
     * @param Bcuerpo Estructura Post
     * @return Cadena de estructura Post en formato Json
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "/v1/psancionados", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String obtenerEstructuraPostFormatoJsonParticularesSancionados(@RequestBody(required = false) String Bcuerpo) throws Exception
    { 
        LogicaDeNegocio logicaNegocio;                              
        FILTRO_PSANCIONADOS_WS particularesSancionadosPost;         
        PSANCIONADOSROOT particularesSancionados;                   
        PSANCIONADOS camposPublicos;                                
        Document documentoDeError;                                  

        logicaNegocio = new LogicaDeNegocioV01();

        try
        {

            Bcuerpo = new String(Bcuerpo.getBytes("ISO-8859-1"), "UTF-8");
            particularesSancionadosPost = new Gson().fromJson(Bcuerpo, FILTRO_PSANCIONADOS_WS.class);
            particularesSancionadosPost = logicaNegocio.obtenerValoresPorDefaultFiltroParticularesSancionados(particularesSancionadosPost);

            if (logicaNegocio.esValidoRFC(particularesSancionadosPost.getRfcSolicitante()) || particularesSancionadosPost.getRfcSolicitante() == null || particularesSancionadosPost.getRfcSolicitante().isEmpty())
            { 

                particularesSancionados = logicaNegocio.obtenerParticularesSancionadosPost(particularesSancionadosPost);
                camposPublicos = logicaNegocio.obtenerEstatusDePrivacidadCamposParticularesSancionados();
                logicaNegocio.guardarBitacoraDeEventos(particularesSancionadosPost, particularesSancionados.getPagination(), "Correcta");

                return logicaNegocio.generarJsonParticularesSancionados(particularesSancionados, camposPublicos, particularesSancionadosPost.getRfcSolicitante());

            } 
            else
            { 

                documentoDeError = new Document();
                documentoDeError.append("code", "psancionados_p02");
                documentoDeError.append("message", "Error el RFC no es válido. " + logicaNegocio.esValidoRFC(particularesSancionadosPost.getRfcSolicitante()));
                logicaNegocio.guardarBitacoraDeEventos(particularesSancionadosPost, null, "El RFC no es válido");

                return documentoDeError.toJson();
            } 
        }
        catch (Exception ex)
        { 

            documentoDeError = new Document();
            documentoDeError.append("code", "psancionados_p01");
            documentoDeError.append("message", "Error al obtener listado de particulares sancionados : " + ex.toString());
            particularesSancionadosPost = logicaNegocio.obtenerValoresPorDefaultFiltroParticularesSancionados(new FILTRO_PSANCIONADOS_WS());
            logicaNegocio.guardarBitacoraDeEventos(particularesSancionadosPost, null, "Error al obtener listado de particulares sancionados : " + ex.toString());

            return documentoDeError.toJson();
        } 
    } 

    /**
     * Obtiene dependecias en formato Json de particulares sancionados
     *
     * @return Cadena de dependencias en formato Json
     */
    @RequestMapping(value = "/v1/psancionados/dependencias", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String obtenerDependenciasParticularesSancionados()
    { 
        LogicaDeNegocio logicaNegocio;              
        Document documentoDeError;                  

        try
        {
            logicaNegocio = new LogicaDeNegocioV01();
            return logicaNegocio.generarJsonDependenciasParticularesSancionados(logicaNegocio.obtenerDependenciasParticularesSancionados());
        }
        catch (Exception ex)
        { 

            documentoDeError = new Document();
            documentoDeError.append("code", "Psancionados_p01");
            documentoDeError.append("message", "Error al obtener listado de Dependencias: " + ex.toString());

            return documentoDeError.toJson();
        } 
    } 

    @RequestMapping(value = "/403", produces = "application/json;charset=UTF-8")
    public String error403()
    {
        Document documentoDeError = new Document();
        documentoDeError.append("code", "403");
        documentoDeError.append("message", "Forbidden");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/404", produces = "application/json;charset=UTF-8")
    public String error404()
    {
        Document documentoDeError = new Document();
        documentoDeError.append("code", "404");
        documentoDeError.append("message", "Not Found");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/405", produces = "application/json;charset=UTF-8")
    public String error405()
    {
        Document documentoDeError = new Document();
        documentoDeError.append("code", "405");
        documentoDeError.append("message", "Method Not Allowed");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/500", produces = "application/json;charset=UTF-8")
    public String error500()
    {
        Document documentoDeError = new Document();
        documentoDeError.append("code", "500");
        documentoDeError.append("message", "Internal Server Error");

        return documentoDeError.toJson();
    }
    
}
