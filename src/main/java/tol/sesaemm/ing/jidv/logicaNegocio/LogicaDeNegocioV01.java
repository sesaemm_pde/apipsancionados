/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaNegocio;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.javabeans.DOCUMENTO;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_QUERY;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_SORT;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost      Estructura para la peticion POST
     * @param strEstatusConsulta  Estatus de la consulta
     * @param valoresDePaginacion Valores de paginacion
     *
     * @throws Exception
     */
    @Override
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {
        DAOWs.guardarBitacoraDeEventos(estructuraPost, valoresDePaginacion, strEstatusConsulta);
    }

    /**
     * Obtener Listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    @Override
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    {
        return DAOWs.obtenerUsuariosTokenDeAcceso();
    }

    /**
     * Obtener valores por default de la estructura post
     *
     * @param estructuraPost Estructura para la peticion POST
     *
     * @return Valores por default de la estructura post
     *
     * @throws Exception
     */
    @Override
    public FILTRO_PSANCIONADOS_WS obtenerValoresPorDefaultFiltroParticularesSancionados(FILTRO_PSANCIONADOS_WS estructuraPost) throws Exception
    {
        if (estructuraPost.getSort() == null)
        {
            estructuraPost.setSort(new FILTRO_PSANCIONADOS_SORT());
        }
        if (estructuraPost.getPage() <= 0)
        {
            estructuraPost.setPage(1);
        }
        if (estructuraPost.getPageSize() <= 0)
        {
            estructuraPost.setPageSize(10);
        }
        else if (estructuraPost.getPageSize() > 200)
        {
            estructuraPost.setPageSize(200);
        }
        if (estructuraPost.getQuery() == null)
        {
            estructuraPost.setQuery(new FILTRO_PSANCIONADOS_QUERY());
        }

        return estructuraPost;
    }

    /**
     * Obtener lista de particulares sancionados
     *
     * @param estructuraPost Estructura POST
     *
     * @return Lista de particulares sancionados
     *
     * @throws Exception
     */
    @Override
    public PSANCIONADOSROOT obtenerParticularesSancionadosPost(FILTRO_PSANCIONADOS_WS estructuraPost) throws Exception
    {
        return DAOWs.obtenerParticularesSancionadosPost(estructuraPost);
    }

    /**
     * Obtener estatus de privacidad campos particulares sancionados
     *
     * @return Estatus de privacidad campos particulares sancionados
     *
     * @throws Exception
     */
    @Override
    public PSANCIONADOS obtenerEstatusDePrivacidadCamposParticularesSancionados() throws Exception
    {
        return DAOWs.obtenerEstatusDePrivacidadCamposParticularesSancionados("psancionados");
    }

    /**
     * Generar formato Json de particulares sancionados
     *
     * @param particularesSancionados
     * @param camposPublicosParticularesSancionados
     * @param rfcSolicitante
     *
     * @return Formato Json de particulares sancionados
     *
     * @throws Exception
     */
    @Override
    public String generarJsonParticularesSancionados(PSANCIONADOSROOT particularesSancionados, PSANCIONADOS camposPublicosParticularesSancionados, String rfcSolicitante) throws Exception
    { 

        Document documentoParticularesSancionados;                                              
        List<Document> listaDocumentosParticularesSancionados = new ArrayList<>();              
        ArrayList<PSANCIONADOS> listaParticularesSancionados;                                   
        PAGINATION atributosDePaginacion;                                                       
        ArrayList<Document> arregloTipoSancion;                                                 
        List<Document> documentos;                                                              

        atributosDePaginacion = particularesSancionados.getPagination();
        listaParticularesSancionados = particularesSancionados.getResults();

        for (PSANCIONADOS particularSancionado : listaParticularesSancionados)
        { 

            documentoParticularesSancionados = new Document();

            documentoParticularesSancionados.append("id", particularSancionado.getIdSpdn().toHexString());

            if (rfcSolicitante != null && !rfcSolicitante.isEmpty())
            { 
                
                documentoParticularesSancionados.append("fechaCaptura", particularSancionado.getFechaCaptura());
                
                if (particularSancionado.getExpediente() != null)
                {
                    documentoParticularesSancionados.append("expediente", particularSancionado.getExpediente());
                }
                
                if (particularSancionado.getInstitucionDependencia() == null)
                { 
                    documentoParticularesSancionados.append("institucionDependencia",
                            new Document()
                                    .append("nombre", null)
                    );
                } 
                else
                { 
                    Document documentoInstitucionDependencia = new Document();
                    documentoInstitucionDependencia.append("nombre", particularSancionado.getInstitucionDependencia().getNombre());
                    if (particularSancionado.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", particularSancionado.getInstitucionDependencia().getSiglas());
                    }
                    if (particularSancionado.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", particularSancionado.getInstitucionDependencia().getClave());
                    }
                    documentoParticularesSancionados.append("institucionDependencia", documentoInstitucionDependencia);
                } 
                
                if (particularSancionado.getParticularSancionado() == null)
                { 
                    documentoParticularesSancionados.append("particularSancionado",
                            new Document()
                                    .append("nombreRazonSocial", null)
                                    .append("tipoPersona", null)
                    );
                } 
                else
                { 
                    Document documentoParticularSancionado = new Document();
                    documentoParticularSancionado.append("nombreRazonSocial", particularSancionado.getParticularSancionado().getNombreRazonSocial());
                    if (particularSancionado.getParticularSancionado().getObjetoSocial() != null)
                    {
                        documentoParticularSancionado.append("objetoSocial", particularSancionado.getParticularSancionado().getObjetoSocial());
                    }
                    if (particularSancionado.getParticularSancionado().getRfc() != null)
                    {
                        documentoParticularSancionado.append("rfc", particularSancionado.getParticularSancionado().getRfc());
                    }
                    documentoParticularSancionado.append("tipoPersona", particularSancionado.getParticularSancionado().getTipoPersona());
                    if (particularSancionado.getParticularSancionado().getTelefono() != null)
                    {
                        documentoParticularSancionado.append("telefono", particularSancionado.getParticularSancionado().getTelefono());
                    }
                    if (particularSancionado.getParticularSancionado().getDomicilioMexico() != null)
                    { 
                        Document documentoDomicilioMexico = new Document();
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getPais() != null)
                        { 
                            Document documentoPais = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getValor() != null)
                            {
                                documentoPais.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getValor());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getClave() != null)
                            {
                                documentoPais.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getClave());
                            }
                            if (!documentoPais.isEmpty())
                            {
                                documentoDomicilioMexico.append("pais", documentoPais);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa() != null)
                        { 
                            Document documentoEntidadFederativa = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getValor() != null)
                            {
                                documentoEntidadFederativa.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getValor());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getClave() != null)
                            {
                                documentoEntidadFederativa.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getClave());
                            }
                            if (!documentoEntidadFederativa.isEmpty())
                            {
                                documentoDomicilioMexico.append("entidadFederativa", documentoEntidadFederativa);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio() != null)
                        { 
                            Document documentoMunicipio = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getValor() != null)
                            {
                                documentoMunicipio.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getValor());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getClave() != null)
                            {
                                documentoMunicipio.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getClave());
                            }
                            if (!documentoMunicipio.isEmpty())
                            {
                                documentoDomicilioMexico.append("municipio", documentoMunicipio);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getCodigoPostal() != null)
                        {
                            documentoDomicilioMexico.append("codigoPostal", particularSancionado.getParticularSancionado().getDomicilioMexico().getCodigoPostal());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad() != null)
                        { 
                            Document documentoLocalidad = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getValor() != null)
                            {
                                documentoLocalidad.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getValor());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getClave() != null)
                            {
                                documentoLocalidad.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getClave());
                            }
                            if (!documentoLocalidad.isEmpty())
                            {
                                documentoDomicilioMexico.append("localidad", documentoLocalidad);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad() != null)
                        { 
                            Document documentoVialidad = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getClave() != null)
                            {
                                documentoVialidad.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getClave());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getValor() != null)
                            {
                                documentoVialidad.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getValor());
                            }
                            if (!documentoVialidad.isEmpty())
                            {
                                documentoDomicilioMexico.append("vialidad", documentoVialidad);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroExterior() != null)
                        {
                            documentoDomicilioMexico.append("numeroExterior", particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroExterior());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroInterior() != null)
                        {
                            documentoDomicilioMexico.append("numeroInterior", particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroInterior());
                        }
                        if (!documentoDomicilioMexico.isEmpty())
                        {
                            documentoParticularSancionado.append("domicilioMexico", documentoDomicilioMexico);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getDomicilioExtranjero() != null)
                    { 
                        Document documentoDomicilioExtranjero = new Document();
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCalle() != null)
                        {
                            documentoDomicilioExtranjero.append("calle", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCalle());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroExterior() != null)
                        {
                            documentoDomicilioExtranjero.append("numeroExterior", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroExterior());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroInterior() != null)
                        {
                            documentoDomicilioExtranjero.append("numeroInterior", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroInterior());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCiudadLocalidad() != null)
                        {
                            documentoDomicilioExtranjero.append("ciudadLocalidad", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCiudadLocalidad());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getEstadoProvincia() != null)
                        {
                            documentoDomicilioExtranjero.append("estadoProvincia", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getEstadoProvincia());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais() != null)
                        { 
                            Document documentoPais = new Document();
                            if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getClave() != null)
                            {
                                documentoPais.append("clave", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getClave());
                            }
                            if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getValor() != null)
                            {
                                documentoPais.append("valor", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getValor());
                            }
                            if (!documentoPais.isEmpty())
                            {
                                documentoDomicilioExtranjero.append("pais", documentoPais);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCodigoPostal() != null)
                        {
                            documentoDomicilioExtranjero.append("codigoPostal", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCodigoPostal());
                        }
                        if (!documentoDomicilioExtranjero.isEmpty())
                        {
                            documentoParticularSancionado.append("domicilioExtranjero", documentoDomicilioExtranjero);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getDirectorGeneral() != null)
                    { 
                        Document documentoDirectorGeneral = new Document();
                        if (particularSancionado.getParticularSancionado().getDirectorGeneral().getNombres() != null)
                        {
                            documentoDirectorGeneral.append("nombres", particularSancionado.getParticularSancionado().getDirectorGeneral().getNombres());
                        }
                        if (particularSancionado.getParticularSancionado().getDirectorGeneral().getPrimerApellido() != null)
                        {
                            documentoDirectorGeneral.append("primerApellido", particularSancionado.getParticularSancionado().getDirectorGeneral().getPrimerApellido());
                        }
                        if (particularSancionado.getParticularSancionado().getDirectorGeneral().getSegundoApellido() != null)
                        {
                            documentoDirectorGeneral.append("segundoApellido", particularSancionado.getParticularSancionado().getDirectorGeneral().getSegundoApellido());
                        }
                        if (particularSancionado.getParticularSancionado().getDirectorGeneral().getCurp() != null)
                        {
                            documentoDirectorGeneral.append("curp", particularSancionado.getParticularSancionado().getDirectorGeneral().getCurp());
                        }
                        if (!documentoDirectorGeneral.isEmpty())
                        {
                            documentoParticularSancionado.append("directorGeneral", documentoDirectorGeneral);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getApoderadoLegal() != null)
                    { 
                        Document documentoApoderadoLegal = new Document();
                        if (particularSancionado.getParticularSancionado().getApoderadoLegal().getNombres() != null)
                        {
                            documentoApoderadoLegal.append("nombres", particularSancionado.getParticularSancionado().getApoderadoLegal().getNombres());
                        }
                        if (particularSancionado.getParticularSancionado().getApoderadoLegal().getPrimerApellido() != null)
                        {
                            documentoApoderadoLegal.append("primerApellido", particularSancionado.getParticularSancionado().getApoderadoLegal().getPrimerApellido());
                        }
                        if (particularSancionado.getParticularSancionado().getApoderadoLegal().getSegundoApellido() != null)
                        {
                            documentoApoderadoLegal.append("segundoApellido", particularSancionado.getParticularSancionado().getApoderadoLegal().getSegundoApellido());
                        }
                        if (particularSancionado.getParticularSancionado().getApoderadoLegal().getCurp() != null)
                        {
                            documentoApoderadoLegal.append("curp", particularSancionado.getParticularSancionado().getApoderadoLegal().getCurp());
                        }
                        if (!documentoApoderadoLegal.isEmpty())
                        {
                            documentoParticularSancionado.append("apoderadoLegal", documentoApoderadoLegal);
                        }
                    } 
                    documentoParticularesSancionados.append("particularSancionado", documentoParticularSancionado);
                } 
                
                if (particularSancionado.getObjetoContrato() != null)
                {
                    documentoParticularesSancionados.append("objetoContrato", particularSancionado.getObjetoContrato());
                }
                
                documentoParticularesSancionados.append("autoridadSancionadora", particularSancionado.getAutoridadSancionadora());
                
                if (particularSancionado.getTipoFalta() != null)
                {
                    documentoParticularesSancionados.append("tipoFalta", particularSancionado.getTipoFalta());
                }
                
                if (particularSancionado.getTipoSancion() == null)
                { 
                    documentoParticularesSancionados.append("tipoSancion",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null)
                    );
                } 
                else
                { 
                    arregloTipoSancion = new ArrayList<>();
                    for (int j = 0; j < particularSancionado.getTipoSancion().size(); j++)
                    { 
                        Document documentoTipoSancion = new Document();
                        
                        documentoTipoSancion.append("clave", particularSancionado.getTipoSancion().get(j).getClave());
                        
                        
                        documentoTipoSancion.append("valor", particularSancionado.getTipoSancion().get(j).getValor());
                        
                        
                        if (particularSancionado.getTipoSancion().get(j).getDescripcion() != null)
                        {
                            documentoTipoSancion.append("descripcion", particularSancionado.getTipoSancion().get(j).getDescripcion());
                        }
                        
                        arregloTipoSancion.add(documentoTipoSancion);
                    } 
                    documentoParticularesSancionados.append("tipoSancion", arregloTipoSancion);
                } 
                
                documentoParticularesSancionados.append("causaMotivoHechos", particularSancionado.getCausaMotivoHechos());
                
                if (particularSancionado.getActo() != null)
                {
                    documentoParticularesSancionados.append("acto", particularSancionado.getActo());
                }
                
                if (particularSancionado.getResponsableSancion() != null)
                { 
                    Document documentoResponsableSancion = new Document();
                    documentoResponsableSancion.append("nombres", particularSancionado.getResponsableSancion().getNombres());
                    documentoResponsableSancion.append("primerApellido", particularSancionado.getResponsableSancion().getPrimerApellido());
                    if (particularSancionado.getResponsableSancion().getSegundoApellido() != null)
                    {
                        documentoResponsableSancion.append("segundoApellido", particularSancionado.getResponsableSancion().getSegundoApellido());
                    }
                    documentoParticularesSancionados.append("responsableSancion", documentoResponsableSancion);
                } 
                
                if (particularSancionado.getResolucion() != null)
                { 
                    Document documentoResolucion = new Document();
                    if (particularSancionado.getResolucion().getSentido() != null)
                    {
                        documentoResolucion.append("sentido", particularSancionado.getResolucion().getSentido());
                    }
                    if (particularSancionado.getResolucion().getUrl() != null)
                    {
                        documentoResolucion.append("url", particularSancionado.getResolucion().getUrl());
                    }
                    if (particularSancionado.getResolucion().getFechaNotificacion() != null)
                    {
                        documentoResolucion.append("fechaNotificacion", particularSancionado.getResolucion().getFechaNotificacion());
                    }
                    if (!documentoResolucion.isEmpty())
                    {
                        documentoParticularesSancionados.append("resolucion", documentoResolucion);
                    }
                } 
                
                if (particularSancionado.getMulta() != null)
                { 
                    Document documentoMulta = new Document();
                    if (particularSancionado.getMulta().getMonto() != null)
                    {
                        documentoMulta.append("monto", Math.round((particularSancionado.getMulta().getMonto() * 100.0)) / 100.0);
                    }
                    if (particularSancionado.getMulta().getMoneda() != null)
                    { 
                        Document documentoMoneda = new Document();
                        if (particularSancionado.getMulta().getMoneda().getClave() != null)
                        {
                            documentoMoneda.append("clave", particularSancionado.getMulta().getMoneda().getClave());
                        }
                        if (particularSancionado.getMulta().getMoneda().getValor() != null)
                        {
                            documentoMoneda.append("valor", particularSancionado.getMulta().getMoneda().getValor());
                        }
                        if (!documentoMoneda.isEmpty())
                        {
                            documentoMulta.append("moneda", documentoMoneda);
                        }
                    } 
                    if (!documentoMulta.isEmpty())
                    {
                        documentoParticularesSancionados.append("multa", documentoMulta);
                    }
                } 
                
                if (particularSancionado.getInhabilitacion() != null)
                { 
                    Document documentoInhabilitacion = new Document();
                    if (particularSancionado.getInhabilitacion().getPlazo() != null)
                    {
                        documentoInhabilitacion.append("plazo", particularSancionado.getInhabilitacion().getPlazo());
                    }
                    if (particularSancionado.getInhabilitacion().getFechaInicial() != null)
                    {
                        documentoInhabilitacion.append("fechaInicial", particularSancionado.getInhabilitacion().getFechaInicial());
                    }
                    if (particularSancionado.getInhabilitacion().getFechaFinal() != null)
                    {
                        documentoInhabilitacion.append("fechaFinal", particularSancionado.getInhabilitacion().getFechaFinal());
                    }
                    if (!documentoInhabilitacion.isEmpty())
                    {
                        documentoParticularesSancionados.append("inhabilitacion", documentoInhabilitacion);
                    }
                } 
                
                if (particularSancionado.getObservaciones() != null)
                {
                    documentoParticularesSancionados.append("observaciones", particularSancionado.getObservaciones());
                }
                
                if (particularSancionado.getDocumentos() != null)
                { 
                    if (particularSancionado.getDocumentos().isEmpty() == false)
                    { 
                        documentos = new ArrayList<>();
                        for (DOCUMENTO documento : particularSancionado.getDocumentos())
                        {
                            Document documentoDocumentos = new Document();
                            if (documento.getId() != null)
                            { 
                                documentoDocumentos.append("id", documento.getId());
                                
                                if (documento.getTipo() != null)
                                {
                                    documentoDocumentos.append("tipo", documento.getTipo());
                                }
                                
                                documentoDocumentos.append("titulo", documento.getTitulo());
                                documentoDocumentos.append("descripcion", documento.getDescripcion());
                                documentoDocumentos.append("url", documento.getUrl());
                                documentoDocumentos.append("fecha", documento.getFecha());
                                
                            } 
                            if (documentoDocumentos.isEmpty() == false)
                            { 
                                documentos.add(documentoDocumentos);
                            } 
                        } 
                        documentoParticularesSancionados.append("documentos", documentos);
                    } 
                    else
                    { 
                        documentoParticularesSancionados.append("documentos", new ArrayList<>());
                    } 
                } 

            } 
            else
            { 

                documentoParticularesSancionados.append("fechaCaptura", particularSancionado.getFechaCaptura());
                
                if (camposPublicosParticularesSancionados.getExpediente().equals("1") && particularSancionado.getExpediente() != null)
                {
                    documentoParticularesSancionados.append("expediente", particularSancionado.getExpediente());
                }
                
                if (particularSancionado.getInstitucionDependencia() == null)
                { 
                    documentoParticularesSancionados.append("institucionDependencia",
                            new Document()
                                    .append("nombre", null));
                } 
                else
                { 
                    Document documentoInstitucionDependencia = new Document();
                    documentoInstitucionDependencia.append("nombre", particularSancionado.getInstitucionDependencia().getNombre());
                    if (camposPublicosParticularesSancionados.getInstitucionDependencia().getSiglas().equals("1") && particularSancionado.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", particularSancionado.getInstitucionDependencia().getSiglas());
                    }
                    if (camposPublicosParticularesSancionados.getInstitucionDependencia().getClave().equals("1") && particularSancionado.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", particularSancionado.getInstitucionDependencia().getClave());
                    }
                    documentoParticularesSancionados.append("institucionDependencia", documentoInstitucionDependencia);
                } 
                
                if (particularSancionado.getParticularSancionado() == null)
                { 
                    documentoParticularesSancionados.append("particularSancionado",
                            new Document()
                                    .append("nombreRazonSocial", null)
                                    .append("tipoPersona", null)
                    );
                } 
                else
                { 
                    Document documentoParticularSancionado = new Document();

                    documentoParticularSancionado.append("nombreRazonSocial", particularSancionado.getParticularSancionado().getNombreRazonSocial());
                    if (camposPublicosParticularesSancionados.getParticularSancionado().getObjetoSocial().equals("1") && particularSancionado.getParticularSancionado().getObjetoSocial() != null)
                    {
                        documentoParticularSancionado.append("objetoSocial", particularSancionado.getParticularSancionado().getObjetoSocial());
                    }
                    if (camposPublicosParticularesSancionados.getParticularSancionado().getRfc().equals("1") && particularSancionado.getParticularSancionado().getRfc() != null)
                    {
                        documentoParticularSancionado.append("rfc", particularSancionado.getParticularSancionado().getRfc());
                    }
                    documentoParticularSancionado.append("tipoPersona", particularSancionado.getParticularSancionado().getTipoPersona());
                    if (camposPublicosParticularesSancionados.getParticularSancionado().getTelefono().equals("1") && particularSancionado.getParticularSancionado().getTelefono() != null)
                    {
                        documentoParticularSancionado.append("telefono", particularSancionado.getParticularSancionado().getTelefono());
                    }
                    if (particularSancionado.getParticularSancionado().getDomicilioMexico() != null)
                    { 
                        Document documentoDomicilioMexico = new Document();
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getPais() != null)
                        { 
                            Document documentoPais = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getPais().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getPais().getValor() != null)
                            {
                                documentoPais.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getValor());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getPais().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getPais().getClave() != null)
                            {
                                documentoPais.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getPais().getClave());
                            }
                            if (!documentoPais.isEmpty())
                            {
                                documentoDomicilioMexico.append("pais", documentoPais);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa() != null)
                        { 
                            Document documentoEntidadFederativa = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getValor() != null)
                            {
                                documentoEntidadFederativa.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getValor());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getClave() != null)
                            {
                                documentoEntidadFederativa.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getEntidadFederativa().getClave());
                            }
                            if (!documentoEntidadFederativa.isEmpty())
                            {
                                documentoDomicilioMexico.append("entidadFederativa", documentoEntidadFederativa);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio() != null)
                        { 
                            Document documentoMunicipio = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getMunicipio().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getMunicipio().getValor() != null)
                            {
                                documentoMunicipio.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getValor());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getMunicipio().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getMunicipio().getClave() != null)
                            {
                                documentoMunicipio.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio().getClave());
                            }
                            if (!documentoMunicipio.isEmpty())
                            {
                                documentoDomicilioMexico.append("municipio", documentoMunicipio);
                            }
                        } 
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getCodigoPostal().equals("1") && particularSancionado.getParticularSancionado().getDomicilioMexico().getCodigoPostal() != null)
                        {
                            documentoDomicilioMexico.append("codigoPostal", particularSancionado.getParticularSancionado().getDomicilioMexico().getCodigoPostal());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio() != null)
                        { 
                            Document documentoLocalidad = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getLocalidad().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getLocalidad().getValor() != null)
                            {
                                documentoLocalidad.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getValor());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getLocalidad().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getLocalidad().getClave() != null)
                            {
                                documentoLocalidad.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getLocalidad().getClave());
                            }
                            if (!documentoLocalidad.isEmpty())
                            {
                                documentoDomicilioMexico.append("localidad", documentoLocalidad);
                            }
                        } 
                        if (particularSancionado.getParticularSancionado().getDomicilioMexico().getMunicipio() != null)
                        { 
                            Document documentoVialidad = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getVialidad().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getVialidad().getClave() != null)
                            {
                                documentoVialidad.append("clave", particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getClave());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getVialidad().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getVialidad().getValor() != null)
                            {
                                documentoVialidad.append("valor", particularSancionado.getParticularSancionado().getDomicilioMexico().getVialidad().getValor());
                            }
                            if (!documentoVialidad.isEmpty())
                            {
                                documentoDomicilioMexico.append("vialidad", documentoVialidad);
                            }
                        } 
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getNumeroExterior().equals("1") && particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroExterior() != null)
                        {
                            documentoDomicilioMexico.append("numeroExterior", particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroExterior());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioMexico().getNumeroInterior().equals("1") && particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroInterior() != null)
                        {
                            documentoDomicilioMexico.append("numeroInterior", particularSancionado.getParticularSancionado().getDomicilioMexico().getNumeroInterior());
                        }

                        if (!documentoDomicilioMexico.isEmpty())
                        {
                            documentoParticularSancionado.append("domicilioMexico", documentoDomicilioMexico);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getDomicilioExtranjero() != null)
                    { 
                        Document documentoDomicilioExtranjero = new Document();
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getCalle().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCalle() != null)
                        {
                            documentoDomicilioExtranjero.append("calle", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCalle());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getNumeroExterior().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroExterior() != null)
                        {
                            documentoDomicilioExtranjero.append("numeroExterior", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroExterior());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getNumeroInterior().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroInterior() != null)
                        {
                            documentoDomicilioExtranjero.append("numeroInterior", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getNumeroInterior());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getCiudadLocalidad().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCiudadLocalidad() != null)
                        {
                            documentoDomicilioExtranjero.append("ciudadLocalidad", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCiudadLocalidad());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getEstadoProvincia().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getEstadoProvincia() != null)
                        {
                            documentoDomicilioExtranjero.append("estadoProvincia", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getEstadoProvincia());
                        }
                        if (particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais() != null)
                        { 
                            Document documentoPais = new Document();
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getPais().getClave().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getPais().getClave() != null)
                            {
                                documentoPais.append("clave", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getClave());
                            }
                            if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getPais().getValor().equals("1") && camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getPais().getValor() != null)
                            {
                                documentoPais.append("valor", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getPais().getValor());
                            }
                            if (!documentoPais.isEmpty())
                            {
                                documentoDomicilioExtranjero.append("pais", documentoPais);
                            }
                        } 
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDomicilioExtranjero().getCodigoPostal().equals("1") && particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCodigoPostal() != null)
                        {
                            documentoDomicilioExtranjero.append("codigoPostal", particularSancionado.getParticularSancionado().getDomicilioExtranjero().getCodigoPostal());
                        }

                        if (!documentoDomicilioExtranjero.isEmpty())
                        {
                            documentoParticularSancionado.append("domicilioExtranjero", documentoDomicilioExtranjero);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getDirectorGeneral() != null)
                    { 
                        Document documentoDirectorGeneral = new Document();
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDirectorGeneral().getNombres().equals("1") && particularSancionado.getParticularSancionado().getDirectorGeneral().getNombres() != null)
                        {
                            documentoDirectorGeneral.append("nombres", particularSancionado.getParticularSancionado().getDirectorGeneral().getNombres());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDirectorGeneral().getPrimerApellido().equals("1") && particularSancionado.getParticularSancionado().getDirectorGeneral().getPrimerApellido() != null)
                        {
                            documentoDirectorGeneral.append("primerApellido", particularSancionado.getParticularSancionado().getDirectorGeneral().getPrimerApellido());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDirectorGeneral().getSegundoApellido().equals("1") && particularSancionado.getParticularSancionado().getDirectorGeneral().getSegundoApellido() != null)
                        {
                            documentoDirectorGeneral.append("segundoApellido", particularSancionado.getParticularSancionado().getDirectorGeneral().getSegundoApellido());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getDirectorGeneral().getCurp().equals("1") && particularSancionado.getParticularSancionado().getDirectorGeneral().getCurp() != null)
                        {
                            documentoDirectorGeneral.append("curp", particularSancionado.getParticularSancionado().getDirectorGeneral().getCurp());
                        }

                        if (!documentoDirectorGeneral.isEmpty())
                        {
                            documentoParticularSancionado.append("directorGeneral", documentoDirectorGeneral);
                        }
                    } 
                    if (particularSancionado.getParticularSancionado().getApoderadoLegal() != null)
                    { 
                        Document documentoApoderadoLegal = new Document();
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getApoderadoLegal().getNombres().equals("1") && particularSancionado.getParticularSancionado().getApoderadoLegal().getNombres() != null)
                        {
                            documentoApoderadoLegal.append("nombres", particularSancionado.getParticularSancionado().getApoderadoLegal().getNombres());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getApoderadoLegal().getPrimerApellido().equals("1") && particularSancionado.getParticularSancionado().getApoderadoLegal().getPrimerApellido() != null)
                        {
                            documentoApoderadoLegal.append("primerApellido", particularSancionado.getParticularSancionado().getApoderadoLegal().getPrimerApellido());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getApoderadoLegal().getSegundoApellido().equals("1") && particularSancionado.getParticularSancionado().getApoderadoLegal().getSegundoApellido() != null)
                        {
                            documentoApoderadoLegal.append("segundoApellido", particularSancionado.getParticularSancionado().getApoderadoLegal().getSegundoApellido());
                        }
                        if (camposPublicosParticularesSancionados.getParticularSancionado().getApoderadoLegal().getCurp().equals("1") && particularSancionado.getParticularSancionado().getApoderadoLegal().getCurp() != null)
                        {
                            documentoApoderadoLegal.append("curp", particularSancionado.getParticularSancionado().getApoderadoLegal().getCurp());
                        }

                        if (!documentoApoderadoLegal.isEmpty())
                        {
                            documentoParticularSancionado.append("apoderadoLegal", documentoApoderadoLegal);
                        }
                    } 

                    documentoParticularesSancionados.append("particularSancionado", documentoParticularSancionado);
                } 
                
                if (camposPublicosParticularesSancionados.getObjetoContrato().equals("1") && particularSancionado.getObjetoContrato() != null)
                {
                    documentoParticularesSancionados.append("objetoContrato", particularSancionado.getObjetoContrato());
                }
                
                documentoParticularesSancionados.append("autoridadSancionadora", particularSancionado.getAutoridadSancionadora());
                
                if (camposPublicosParticularesSancionados.getTipoFalta().equals("1") && particularSancionado.getTipoFalta() != null)
                {
                    documentoParticularesSancionados.append("tipoFalta", particularSancionado.getTipoFalta());
                }
                
                if (particularSancionado.getTipoSancion() == null)
                { 
                    documentoParticularesSancionados.append("tipoSancion",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null)
                    );
                } 
                else
                { 
                    arregloTipoSancion = new ArrayList<>();
                    for (int j = 0; j < particularSancionado.getTipoSancion().size(); j++)
                    { 
                        Document documentoTipoSancion = new Document();
                        
                        documentoTipoSancion.append("clave", particularSancionado.getTipoSancion().get(j).getClave());
                        
                        
                        documentoTipoSancion.append("valor", particularSancionado.getTipoSancion().get(j).getValor());
                        
                        
                        if (camposPublicosParticularesSancionados.getTipoSancion().get(0).getDescripcion().equals("1") && particularSancionado.getTipoSancion().get(j).getDescripcion() != null)
                        {
                            documentoTipoSancion.append("descripcion", particularSancionado.getTipoSancion().get(j).getDescripcion());
                        }
                        
                        arregloTipoSancion.add(documentoTipoSancion);
                    } 
                    documentoParticularesSancionados.append("tipoSancion", arregloTipoSancion);
                } 
                
                documentoParticularesSancionados.append("causaMotivoHechos", particularSancionado.getCausaMotivoHechos());
                
                if (camposPublicosParticularesSancionados.getActo().equals("1") && particularSancionado.getActo() != null)
                {
                    documentoParticularesSancionados.append("acto", particularSancionado.getActo());
                }
                
                if (particularSancionado.getResponsableSancion() != null)
                { 
                    Document documentoResponsableSancion = new Document();
                    documentoResponsableSancion.append("nombres", particularSancionado.getResponsableSancion().getNombres());
                    documentoResponsableSancion.append("primerApellido", particularSancionado.getResponsableSancion().getPrimerApellido());
                    if (camposPublicosParticularesSancionados.getResponsableSancion().getSegundoApellido().equals("1") && particularSancionado.getResponsableSancion().getSegundoApellido() != null)
                    {
                        documentoResponsableSancion.append("segundoApellido", particularSancionado.getResponsableSancion().getSegundoApellido());
                    }

                    documentoParticularesSancionados.append("responsableSancion", documentoResponsableSancion);
                } 
                
                if (particularSancionado.getResolucion() != null)
                { 
                    Document documentoResolucion = new Document();
                    if (camposPublicosParticularesSancionados.getResolucion().getSentido().equals("1") && particularSancionado.getResolucion().getSentido() != null)
                    {
                        documentoResolucion.append("sentido", particularSancionado.getResolucion().getSentido());
                    }
                    if (camposPublicosParticularesSancionados.getResolucion().getUrl().equals("1") && particularSancionado.getResolucion().getUrl() != null)
                    {
                        documentoResolucion.append("url", particularSancionado.getResolucion().getUrl());
                    }
                    if (camposPublicosParticularesSancionados.getResolucion().getFechaNotificacion().equals("1") && particularSancionado.getResolucion().getFechaNotificacion() != null)
                    {
                        documentoResolucion.append("fechaNotificacion", particularSancionado.getResolucion().getFechaNotificacion());
                    }

                    if (!documentoResolucion.isEmpty())
                    {
                        documentoParticularesSancionados.append("resolucion", documentoResolucion);
                    }
                } 
                
                if (particularSancionado.getMulta() != null)
                { 
                    Document documentoMulta = new Document();
                    if (camposPublicosParticularesSancionados.getMulta().getMonto() == 1 && particularSancionado.getMulta().getMonto() != null)
                    {
                        documentoMulta.append("monto", Math.round((particularSancionado.getMulta().getMonto() * 100.0)) / 100.0);
                    }
                    if (camposPublicosParticularesSancionados.getMulta().getMoneda() != null)
                    { 
                        Document documentoMoneda = new Document();
                        if (camposPublicosParticularesSancionados.getMulta().getMoneda().getClave().equals("1") && particularSancionado.getMulta().getMoneda().getClave() != null)
                        {
                            documentoMoneda.append("clave", particularSancionado.getMulta().getMoneda().getClave());
                        }
                        if (camposPublicosParticularesSancionados.getMulta().getMoneda().getValor().equals("1") && particularSancionado.getMulta().getMoneda().getValor() != null)
                        {
                            documentoMoneda.append("valor", particularSancionado.getMulta().getMoneda().getValor());
                        }

                        if (!documentoMoneda.isEmpty())
                        {
                            documentoMulta.append("moneda", documentoMoneda);
                        }
                    } 

                    if (!documentoMulta.isEmpty())
                    {
                        documentoParticularesSancionados.append("multa", documentoMulta);
                    }
                } 
                
                if (particularSancionado.getInhabilitacion() != null)
                { 
                    Document documentoInhabilitacion = new Document();
                    if (camposPublicosParticularesSancionados.getInhabilitacion().getPlazo().equals("1") && particularSancionado.getInhabilitacion().getPlazo() != null)
                    {
                        documentoInhabilitacion.append("plazo", particularSancionado.getInhabilitacion().getPlazo());
                    }
                    if (camposPublicosParticularesSancionados.getInhabilitacion().getFechaInicial().equals("1") && particularSancionado.getInhabilitacion().getFechaInicial() != null)
                    {
                        documentoInhabilitacion.append("fechaInicial", particularSancionado.getInhabilitacion().getFechaInicial());
                    }
                    if (camposPublicosParticularesSancionados.getInhabilitacion().getFechaFinal().equals("1") && particularSancionado.getInhabilitacion().getFechaFinal() != null)
                    {
                        documentoInhabilitacion.append("fechaFinal", particularSancionado.getInhabilitacion().getFechaFinal());
                    }

                    if (!documentoInhabilitacion.isEmpty())
                    {
                        documentoParticularesSancionados.append("inhabilitacion", documentoInhabilitacion);
                    }
                } 
                
                if (camposPublicosParticularesSancionados.getObservaciones().equals("1") && particularSancionado.getObservaciones() != null)
                {
                    documentoParticularesSancionados.append("observaciones", particularSancionado.getObservaciones());
                }
                
                if (particularSancionado.getDocumentos() != null)
                { 
                    if (particularSancionado.getDocumentos().isEmpty() == false)
                    { 
                        documentos = new ArrayList<>();
                        for (DOCUMENTO documento : particularSancionado.getDocumentos())
                        {
                            Document documentoDocumentos = new Document();
                            if (documento.getId() != null)
                            { 
                                
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getId().equals("1"))
                                {
                                    documentoDocumentos.append("id", documento.getId());
                                }
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getTipo().equals("1") && documento.getTipo() != null)
                                {
                                    documentoDocumentos.append("tipo", documento.getTipo());
                                }
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getTitulo().equals("1"))
                                {
                                    documentoDocumentos.append("titulo", documento.getTitulo());
                                }
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getDescripcion().equals("1"))
                                {
                                    documentoDocumentos.append("descripcion", documento.getDescripcion());
                                }
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getUrl().equals("1"))
                                {
                                    documentoDocumentos.append("url", documento.getUrl());
                                }
                                if (camposPublicosParticularesSancionados.getDocumentos().get(0).getFecha().equals("1"))
                                {
                                    documentoDocumentos.append("fecha", documento.getFecha());
                                }
                                
                            } 
                            if (documentoDocumentos.isEmpty() == false)
                            { 
                                documentos.add(documentoDocumentos);
                            } 
                        } 
                        documentoParticularesSancionados.append("documentos", documentos);
                    } 
                    else
                    { 
                        documentoParticularesSancionados.append("documentos", new ArrayList<>());
                    } 
                } 

            } 
            listaDocumentosParticularesSancionados.add(documentoParticularesSancionados);
        } 

        documentoParticularesSancionados = new Document();
        documentoParticularesSancionados.append("pagination",
                new Document("pageSize", atributosDePaginacion.getPageSize())
                        .append("page", atributosDePaginacion.getPage())
                        .append("totalRows", atributosDePaginacion.getTotalRows())
                        .append("hasNextPage", atributosDePaginacion.getHasNextPage()));

        documentoParticularesSancionados.append("results", listaDocumentosParticularesSancionados);
        return documentoParticularesSancionados.toJson();
    }

    /**
     * Obtener dependencias de particulares sancionados
     *
     * @return Dependencias de particulares sancionados
     *
     * @throws Exception
     */
    @Override
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasParticularesSancionados() throws Exception
    {
        return DAOWs.obtenerDependenciasParticularesSancionados();
    }

    /**
     * Generar en formato Json dependencias particulares sancionados
     *
     * @param dependenciasParticularesSancionados
     *
     * @return Formato Json dependencias particulares sancionados
     *
     * @throws Exception
     */
    @Override
    public String generarJsonDependenciasParticularesSancionados(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasParticularesSancionados) throws Exception
    {

        List<Document> listaDocumentosDependencias = new ArrayList<>();         
        Document documentoDependencia;                                          

        for (INSTITUCION_DEPENDENCIA dependencia : dependenciasParticularesSancionados)
        { 

            documentoDependencia = new Document();
            documentoDependencia.append("nombre", dependencia.getNombre());
            
            if (dependencia.getSiglas() != null)
            {
                documentoDependencia.append("siglas", dependencia.getSiglas());
            }
            if (dependencia.getClave() != null)
            {
                documentoDependencia.append("clave", dependencia.getClave());
            }
            
            listaDocumentosDependencias.add(documentoDependencia);
        }
        return new Gson().toJson(listaDocumentosDependencias);
    }

    
    /**
     * Convierte una cadena, en una expresión regular que contiene o no, acentos
     *
     * @param palabra
     *
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    {

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        { 
            switch (letra)
            {
                case 'a':
                    expReg += "[aáAÁ]";
                    break;
                case 'á':
                    expReg += "[aáAÁ]";
                    break;
                case 'e':
                    expReg += "[eéEÉ]";
                    break;
                case 'é':
                    expReg += "[eéEÉ]";
                    break;
                case 'i':
                    expReg += "[iíIÍ]";
                    break;
                case 'í':
                    expReg += "[iíIÍ]";
                    break;
                case 'o':
                    expReg += "[oóOÓ]";
                    break;
                case 'ó':
                    expReg += "[oóOÓ]";
                    break;
                case 'u':
                    expReg += "[uúUÚ]";
                    break;
                case 'ú':
                    expReg += "[uúUÚ]";
                    break;
                default:
                    expReg += letra;
                    break;
                
            }
        } 
        return expReg;
    }

    /**
     * Valida RFC
     *
     * @param rfc RFC
     *
     * @return Falso o verdadero
     *
     * @throws Exception
     */
    @Override
    public boolean esValidoRFC(String rfc) throws Exception
    {
        return DAOWs.esValidoRFC(rfc);
    }
}
