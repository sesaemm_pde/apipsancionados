/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public interface LogicaDeNegocio
{    
    
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion ,String strEstatusConsulta) throws Exception;
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception;
    public FILTRO_PSANCIONADOS_WS obtenerValoresPorDefaultFiltroParticularesSancionados (FILTRO_PSANCIONADOS_WS estructuraPost) throws Exception;    
    public PSANCIONADOSROOT obtenerParticularesSancionadosPost(FILTRO_PSANCIONADOS_WS estructuraPost) throws Exception;
    public PSANCIONADOS obtenerEstatusDePrivacidadCamposParticularesSancionados() throws Exception;
    public String generarJsonParticularesSancionados(PSANCIONADOSROOT particularesSancionados, PSANCIONADOS camposPublicosparticularesSancionados, String rfcSolicitante) throws Exception;
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasParticularesSancionados() throws Exception;
    public String generarJsonDependenciasParticularesSancionados(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasParticularesSancionados) throws Exception;        
    public String crearExpresionRegularDePalabra(String palabra);
    public boolean esValidoRFC(String rfc) throws Exception;  
    
}