/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.Gson;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3psancionados.FILTRO_PSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOS;
import tol.sesaemm.javabeans.s3psancionados.PSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOWs
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost      Estructura para la peticion POST
     * @param strEstatusConsulta  Estatus de la consulta
     * @param valoresDePaginacion Valores de paginacion
     *
     * @throws Exception
     */
    public static void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                      
        MongoDatabase database;                                             
        MongoCollection<Document> coleccion;                                
        FILTRO_PSANCIONADOS_WS estructuraPostPSancionados;                  
        PAGINATION valoresDePaginacionPSancionados;                         
        ArrayList<Document> arregloListaFiltrosDeBusqueda;                  
        Document documentoBitacora;                                         
        Date fecha;                                                         
        SimpleDateFormat formateador;                                       
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        fecha = new Date();
        formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                coleccion = database.getCollection("bitacora_servicios_web");
                arregloListaFiltrosDeBusqueda = new ArrayList<>();
                documentoBitacora = new Document();

                if (estructuraPost instanceof FILTRO_PSANCIONADOS_WS)
                { 

                    estructuraPostPSancionados = (FILTRO_PSANCIONADOS_WS) estructuraPost;

                    if (estructuraPostPSancionados.getQuery().getNombreRazonSocial() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "nombreRazonSocial")
                                .append("valor", estructuraPostPSancionados.getQuery().getNombreRazonSocial())
                        );
                    }
                    if (estructuraPostPSancionados.getQuery().getRfc() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "rfc")
                                .append("valor", estructuraPostPSancionados.getQuery().getRfc())
                        );
                    }
                    if (estructuraPostPSancionados.getQuery().getInstitucionDependencia() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "institucionDependencia")
                                .append("valor", estructuraPostPSancionados.getQuery().getInstitucionDependencia())
                        );
                    }
                    if (estructuraPostPSancionados.getQuery().getExpediente() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "expediente")
                                .append("valor", estructuraPostPSancionados.getQuery().getExpediente())
                        );
                    }
                    if (estructuraPostPSancionados.getQuery().getTipoSancion() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "tipoSancion")
                                .append("valor", estructuraPostPSancionados.getQuery().getTipoSancion())
                        );
                    }
                    if (estructuraPostPSancionados.getQuery().getTipoPersona() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "tipoPersona")
                                .append("valor", estructuraPostPSancionados.getQuery().getTipoPersona())
                        );
                    }
                    if (estructuraPostPSancionados.getRfcSolicitante() != null)
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", estructuraPostPSancionados.getRfcSolicitante())
                                .append("privacidadConsulta", "PRV")
                                .append("sistemaConsultado", "Particulares Sancionados")
                        );
                    } 
                    else
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", (estructuraPostPSancionados.getRfcSolicitante() != null ? estructuraPostPSancionados.getRfcSolicitante() : ""))
                                .append("privacidadConsulta", "PUB")
                                .append("sistemaConsultado", "Particulares Sancionados")
                        );
                    } 
                    if (valoresDePaginacion != null)
                    { 

                        valoresDePaginacionPSancionados = (PAGINATION) valoresDePaginacion;

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", valoresDePaginacionPSancionados.getTotalRows())
                        );
                    } 
                    else
                    { 

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", 0)
                        );
                    } 
                    
                } 
                
                coleccion.insertOne(documentoBitacora);
                
            }
            else
            { 
                throw new Exception("Conexion no establecida");
            } 

        }
        catch (Exception ex)
        { 
            throw new Exception("Error en bitacora de eventos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

    }

    /**
     * Obtener Listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    public static ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    {

        ArrayList<USUARIO_TOKEN_ACCESO> arregloUsuariosTokenAcceso;             
        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<Document> query;                                              
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios_token_acceso");
                query = new ArrayList<>();
                query.add(new Document("$match", new Document("activo", new Document("$eq", 1))));
                arregloUsuariosTokenAcceso = collection.aggregate(query, USUARIO_TOKEN_ACCESO.class).collation(collation).into(new ArrayList<>());
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener usuarios token acceso: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        } 

        return arregloUsuariosTokenAcceso;
    }

    
    /**
     * Obtener lista de particulares sancionados
     *
     * @param estructuraPost Estructura POST
     *
     * @return Lista de particulares sancionados
     *
     * @throws Exception
     */
    public static PSANCIONADOSROOT obtenerParticularesSancionadosPost(FILTRO_PSANCIONADOS_WS estructuraPost) throws Exception
    { 

        LogicaDeNegocio logicaNegocio = new LogicaDeNegocioV01();                       
        ArrayList<PSANCIONADOS> listaParticularesSancionados;                           
        PAGINATION atributosDePaginacion;                                               
        PSANCIONADOSROOT particularesSancionados;                                       
        MongoClient conectarBaseDatos = null;                                                  
        MongoDatabase database;                                                         
        MongoCollection<Document> collection;                                           
        AggregateIterable<Document> iteradorDeResultados;                               
        MongoCursor<Document> cursorDeResultados = null;                                       
        Document where = new Document();                                                
        Document orden = new Document();                                                
        int intNumeroDocumentosEncontrados = 0;                                         
        int intNumeroSaltos = 0;                                                        
        boolean hasNextPage = false;                                                    
        Collation collation;                                                            
        ArrayList<Document> arregloTipoSancionClave = new ArrayList<>();                
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("psancionados");
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                
                if (estructuraPost.getQuery().getId() != null && !estructuraPost.getQuery().getId().isEmpty())
                { 
                    where.append("identificador", estructuraPost.getQuery().getId());
                } 
                else
                { 
                    if (estructuraPost.getQuery().getNombreRazonSocial() != null && !estructuraPost.getQuery().getNombreRazonSocial().isEmpty())
                    { 
                        where.append("particularSancionado.nombreRazonSocial",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getNombreRazonSocial())).append("$options", "i"));
                    } 
                    if (estructuraPost.getQuery().getRfc() != null && !estructuraPost.getQuery().getRfc().isEmpty())
                    { 
                        where.append("particularSancionado.rfc",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getRfc())).append("$options", "i"));
                    } 
                    if (estructuraPost.getQuery().getInstitucionDependencia() != null && !estructuraPost.getQuery().getInstitucionDependencia().isEmpty())
                    { 
                        where.append("institucionDependencia.nombre",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getInstitucionDependencia())).append("$options", "i"));
                    } 
                    if (estructuraPost.getQuery().getExpediente() != null && !estructuraPost.getQuery().getExpediente().isEmpty())
                    { 
                        where.append("expediente",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getExpediente())).append("$options", "i"));
                    } 
                    if (estructuraPost.getQuery().getTipoSancion() != null && !estructuraPost.getQuery().getTipoSancion().isEmpty())
                    { 

                        for (int j = 0; j < estructuraPost.getQuery().getTipoSancion().size(); j++)
                        { 
                            arregloTipoSancionClave.add(new Document("tipoSancion.clave", estructuraPost.getQuery().getTipoSancion().get(j)));
                        } 

                        where.append("$or", arregloTipoSancionClave);
                    } 
                    if (estructuraPost.getQuery().getTipoPersona() != null && !estructuraPost.getQuery().getTipoPersona().isEmpty())
                    { 
                        where.append("particularSancionado.tipoPersona",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getTipoPersona())).append("$options", "i"));
                    } 
                } 

                where.append("publicar", new Document("$ne", "0"));
                
                if (estructuraPost.getSort().getNombreRazonSocial() != null && !estructuraPost.getSort().getNombreRazonSocial().isEmpty())
                { 
                    if(estructuraPost.getSort().getNombreRazonSocial().toLowerCase().equals("desc"))
                    {
                        orden.append("data.particularSancionado.nombreRazonSocial", -1);                   
                    }
                    else if(estructuraPost.getSort().getNombreRazonSocial().toLowerCase().equals("asc"))
                    {
                        orden.append("data.particularSancionado.nombreRazonSocial", 1);
                    }
                } 
                if (estructuraPost.getSort().getRfc() != null && !estructuraPost.getSort().getRfc().isEmpty())
                { 
                    if(estructuraPost.getSort().getRfc().toLowerCase().equals("desc"))
                    {
                        orden.append("data.particularSancionado.rfc", -1);
                    }
                    else if(estructuraPost.getSort().getRfc().toLowerCase().equals("asc"))
                    {
                        orden.append("data.particularSancionado.rfc", 1);                        
                    }
                } 
                if (estructuraPost.getSort().getInstitucionDependencia() != null && !estructuraPost.getSort().getInstitucionDependencia().isEmpty())
                { 
                    if(estructuraPost.getSort().getInstitucionDependencia().toLowerCase().equals("desc"))
                    {
                        orden.append("data.institucionDependencia.nombre", -1);
                    }
                    else if(estructuraPost.getSort().getInstitucionDependencia().toLowerCase().equals("asc"))
                    {
                        orden.append("data.institucionDependencia.nombre", 1);
                    }
                } 
                if (orden.isEmpty())
                { 
                    orden.append("data.fechaCaptura", 1);
                } 
                
                iteradorDeResultados = collection.aggregate(
                        Arrays.asList(
                                new Document("$addFields",
                                        new Document("fechaCaptura", "$fechaCaptura")
                                                .append("ejercicioFiscal", "$ejercicioFiscal")
                                                .append("identificador", new Document()
                                                        .append("$convert", new Document()
                                                                .append("input", "$_id")
                                                                .append("to", "string")
                                                                .append("onError", "")
                                                                .append("onNull", "")
                                                        )
                                                )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$count", "totalCount")
                        )
                ).collation(collation).allowDiskUse(true);
                
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    intNumeroDocumentosEncontrados = cursorDeResultados.next().getInteger("totalCount");
                } 
                
                if (estructuraPost.getPage() > 1)
                { 
                    intNumeroSaltos = (int) ((estructuraPost.getPage() - 1) * estructuraPost.getPageSize());
                } 
                if ((estructuraPost.getPage() * estructuraPost.getPageSize()) < intNumeroDocumentosEncontrados)
                { 
                    hasNextPage = true;
                } 

                listaParticularesSancionados = collection.aggregate(
                        Arrays.asList(
                                new Document("$addFields",
                                        new Document("fechaCaptura", "$fechaCaptura")
                                                .append("expediente", "$expediente")
                                                .append("identificador", new Document()
                                                        .append("$convert", new Document()
                                                                .append("input", "$_id")
                                                                .append("to", "string")
                                                                .append("onError", "")
                                                                .append("onNull", "")
                                                        )
                                                )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$project",
                                        new Document("data", "$$ROOT")
                                ),
                                new Document("$sort",
                                        orden
                                ),
                                new Document("$skip", intNumeroSaltos),
                                new Document("$limit", estructuraPost.getPageSize()),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$data")
                                )
                        ), PSANCIONADOS.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
            
                atributosDePaginacion = new PAGINATION();
                atributosDePaginacion.setPage(estructuraPost.getPage());
                atributosDePaginacion.setPageSize(estructuraPost.getPageSize());
                atributosDePaginacion.setTotalRows(intNumeroDocumentosEncontrados);
                atributosDePaginacion.setHasNextPage(hasNextPage);
                
                particularesSancionados = new PSANCIONADOSROOT();
                particularesSancionados.setPagination(atributosDePaginacion);
                particularesSancionados.setResults(listaParticularesSancionados);
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener particulares sancionados: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return particularesSancionados;
    } 

    /**
     * Obtener estatus de provacidad campos particulares sancionados
     *
     * @param coleccion Coleccion
     *
     * @return Estatus de provacidad campos particulares sancionados
     *
     * @throws Exception
     */
    public static PSANCIONADOS obtenerEstatusDePrivacidadCamposParticularesSancionados(String coleccion) throws Exception
    { 

        PSANCIONADOS estatusDePrivacidad;                               
        MongoClient conectarBaseDatos = null;                                  
        MongoDatabase database;                                         
        MongoCollection<Document> collection;                           
        Document documentoCamposPublicosPSANCIONADOS;                   
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            estatusDePrivacidad = new PSANCIONADOS();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("campos_publicos");

                documentoCamposPublicosPSANCIONADOS = collection.find(new Document("coleccion", coleccion)).first();
                if (documentoCamposPublicosPSANCIONADOS != null)
                { 
                    estatusDePrivacidad = new Gson().fromJson(documentoCamposPublicosPSANCIONADOS.toJson(), PSANCIONADOS.class);
                } 
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return estatusDePrivacidad;
    } 

    /**
     * Obtener dependencias de particulares sancionados
     *
     * @return Dependencias de particulares sancionados
     *
     * @throws Exception
     */
    public static ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasParticularesSancionados() throws Exception
    { 

        ArrayList<INSTITUCION_DEPENDENCIA> listaDependenciasParticularesSancionados;                        
        MongoClient conectarBaseDatos = null;                                                                      
        MongoDatabase database;                                                                             
        MongoCollection<Document> collection;                                                               
        Collation collation;                                                                                
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            listaDependenciasParticularesSancionados = new ArrayList<>();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("psancionados");
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                
                listaDependenciasParticularesSancionados = collection.aggregate(
                        Arrays.asList(
                                new Document("$project", new Document("nombre", "$institucionDependencia.nombre")
                                        .append("siglas", "$institucionDependencia.siglas")
                                        .append("clave", "$institucionDependencia.clave")
                                ),
                                new Document("$group",
                                        new Document("_id", new Document("nombre", "$nombre")
                                                .append("siglas", "$siglas")
                                                .append("clave", "$clave"))
                                ),
                                new Document("$sort", new Document("_id.nombre", 1)
                                ),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$_id")
                                )
                        ), INSTITUCION_DEPENDENCIA.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener las Dependencias : " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaDependenciasParticularesSancionados;
    } 

    /**
     * Verifica si el RFC es valido
     *
     * @param rfc RFC a validar
     *
     * @return Verdadero o falso
     *
     * @throws Exception
     */
    public static boolean esValidoRFC(String rfc) throws Exception
    { 

        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> iteradorDeResultados;                            
        MongoCursor<Document> cursorDeResultados = null;                               
        boolean esValido = false;                                               
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios");
                iteradorDeResultados = collection.find(new Document("rfc", rfc));
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    esValido = true;
                } 
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return esValido;
    } 

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    {
        
        if(cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 
        
        if(conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        }
        
    }
}
